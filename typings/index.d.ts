export declare const promisify: typeof _promisify;
declare function _promisify<T extends any>(func: T): MpApiAsyncFunc<T>;
export declare function promisifyAll<T extends any>(api: T, methods?: string[]): OmitNever<MpApiAsyncAll<T>>;
export {};
