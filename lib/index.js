export const promisify = _promisify;
const _cacheFunc = new Map();
function _promisify(func) {
    let asyncFunc = _cacheFunc.get(func);
    if (typeof asyncFunc === "function") {
        return _cacheFunc.get(func);
    }
    if (typeof func !== "function") {
        return func;
    }
    asyncFunc = ((args = {}) => new Promise((resolve, reject) => {
        func(Object.assign(args, {
            success: resolve,
            fail: reject
        }));
    }));
    _cacheFunc.set(func, asyncFunc);
    return asyncFunc;
}
export function promisifyAll(api, methods) {
    const result = {};
    let keys = Object.keys(api);
    if (methods && methods.length > 0) {
        keys = methods;
    }
    let fn;
    for (const key of keys) {
        fn = api[key];
        if (typeof fn === 'function') {
            result[key] = _promisify(fn);
        }
    }
    return result;
}
