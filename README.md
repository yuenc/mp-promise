# mp-promise

api promisify

# Installation

```
npm install --save mp-promise
```

# Getting started

💨example:
``` typescript
import { promisifyAll, promisify } from 'mp-promise';

const asyncApi = promisifyAll(api)

const asyncFunc = promisify(func)

```

# WechatMiniprogram Example

``` typescript
// typings/index.d.ts
/// <reference path="../node_modules/mp-promise/types/index.d.ts" />

type WxApi = Omit<WechatMiniprogram.Wx, "wxp">;
declare namespace WechatMiniprogram {
  interface Wx {
    wxp: OmitNever<MpApiAsyncAll<WxApi>>;
  }
}

// miniprogram/app.ts
import { promisifyAll } from "mp-promise";
wx.wxp = promisifyAll(wx);

```
