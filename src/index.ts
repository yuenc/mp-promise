export const promisify = _promisify;
const _cacheFunc: Map<any, any> = new Map();
function _promisify<T extends any>(func: T): MpApiAsyncFunc<T> {
  let asyncFunc = _cacheFunc.get(func);
  if (typeof asyncFunc === "function") {
    return _cacheFunc.get(func);
  }
  if (typeof func !== "function") {
    return func as any;
  }
  asyncFunc = ((args: any = {}) =>
    new Promise((resolve, reject) => {
      func(
        Object.assign(args, {
          success: resolve,
          fail: reject
        })
      );
    }));
  _cacheFunc.set(func, asyncFunc);
  return asyncFunc;
}

export function promisifyAll<T extends any>(api: T, methods?: string[]): OmitNever<MpApiAsyncAll<T>> {
  const result: any = {};
  let keys = Object.keys(api);
  if (methods && methods.length > 0) {
    keys = methods;
  }
  let fn: any;
  for (const key of keys) {
    fn = api[key];
    if (typeof fn === 'function') {
      result[key] = _promisify(fn);
    }
  }
  return result;
}
