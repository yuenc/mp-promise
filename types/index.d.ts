type OmitNever<T> = Pick<T, { [P in keyof T]: T[P] extends never ? never : P }[keyof T]>;

type MpApiAsyncFunc<T extends any> = T extends ((option: infer O) => void)
  ? O extends { success?: (arg: infer T) => void }
  ? (option: O) => Promise<T>
  : never
  : never;

type MpApiAsyncAll<T extends Record<string, any>> = {
  [K in keyof T]: MpApiAsyncFunc<T[K]>;
}
